from pydantic import BaseSettings


class Settings(BaseSettings):
    debug: bool = True

    wait_for_rabbit: bool = False

    redis_host: str = "localhost"
    redis_port: int = 6379

    rabbit_user: str = "guest"
    rabbit_password: str = "guest"
    rabbit_port: str = "5672"
    rabbit_host: str = "localhost"
    rabbit_queue: str = "StockAnalysisData"
