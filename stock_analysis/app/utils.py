import numpy as np
import tensorflow as tf
from hurst import compute_Hc
from datetime import datetime
from typing import Tuple, List, Dict, Any
from sklearn.preprocessing import MinMaxScaler

from .models import CandlesModel


def get_hurst_exponent(time_series: List[float]) -> float:
    """ Returns the Hurst Exponent of the time series
    """
    h, _, _ = compute_Hc(time_series, kind='price', simplified=True)
    return h


def scale_data(X: np.array, y: np.array) -> Tuple[np.array, np.array]:
    scaler_x = MinMaxScaler(feature_range = (0,1))
    scaler_y = MinMaxScaler(feature_range = (0,1))

    # Fit the scaler using available training data
    input_scaler = scaler_x.fit(X)
    output_scaler = scaler_y.fit(y)

    # Apply the scaler to training data
    return (
        output_scaler.transform(X),
        input_scaler.transform(y)
    )
    

def replace_outliers(
            data: np.array,
            max_deviation: int = 2
        ) -> Tuple[np.array, np.array]:
    data_mean = np.mean(data)
    data_std = np.std(data)
    distance_from_mean = abs(data - data_mean)
    proper_ids = distance_from_mean < max_deviation * data_std
    last_proper = data[0]
    
    for i, replace in enumerate(proper_ids):
        if not replace:
            data[i] = last_proper
        else:
            last_proper = data[i]
    return data


def prepare_data(candles: CandlesModel):
    data = np.array([
        [
            (
                datetime.strptime(c.time, "%Y-%m-%d %H:%M:%S") \
                - datetime(1970, 1, 1)
            ).total_seconds(), 
            c.openPrice
        ] for c in candles.data
    ]).T
    dates = data[0]
    opens = replace_outliers(data[1])
    return scale_data(dates, opens)


def fit_data(
            model,
            X: np.array,
            y: np.array,
            barch_size: int = 32,
            epochs: int = 300,
            validation_split: float = 0.3
        ):
    early_stop = tf.keras.callbacks.EarlyStopping(
        monitor = 'val_loss',
        patience = 10
    )
    model.fit(
        X, 
        y, 
        epochs = epochs,  
        validation_split = validation_split,
        batch_size = barch_size,  
        verbose = 0,
        shuffle = False,
        callbacks = [early_stop]
    )
    return model


def create_model(
            units: int,
            input_shape: Tuple[int, int]
        ):
    model = tf.keras.Sequential()
    model.add(tf.keras.LSTM(
        units = units, 
        return_sequences = True,
        input_shape=input_shape
    ))
    model.add(tf.keras.Dropout(0.2))
    model.add(tf.keras.LSTM(units=units))
    model.add(tf.keras.Dropout(0.2))
    model.add(tf.keras.Dense(units=1))

    model.compile(loss='mse', optimizer='adam')
    
    return model


def predict_data(candles: CandlesModel):
    X, y = prepare_data(candles)
    model = create_model(
        units=50,
        input_shape=(
            x.shape[1]
        )
    )
    model = fit_data(
        model,
        X,
        y
    )
    return model

def save_model():
    pass

def load_model():
    pass