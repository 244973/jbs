import json
from kombu.mixins import ConsumerMixin
from datetime import datetime
from .models import CandlesModel, AnalysisModel, HurstModel, HurstPeriodValue
from . import app_settings, app_logger
from .utils import get_hurst_exponent

class SAConsumer(ConsumerMixin):

    def __init__(self, connection, redis_con, queue):
        self.connection = connection
        self.redis_con = redis_con
        self.queue = queue


    def on_consume_ready(self, connection, channel, consumers, **kwargs):
        app_logger.info("Consumer ready for accept messages.")
        return super().on_consume_ready(connection, channel, consumers, **kwargs)


    def get_consumers(self, Consumer, channel):
        return [Consumer(queues=[self.queue], callbacks=[self._on_message], accept=['json'])]


    def _on_message(self, body, message):
        app_logger.info(f"Consumer recived message.")
        error = False
        # serialize messsage
        try:
            json_data = json.loads(body)
            candles = CandlesModel(**json_data)
            app_logger.info(f"Message data with ticker {candles.ticker} contain {len(candles.data)} enities.")
        except Exception as e:
            app_logger.error(f"Decode message error: {e}")
            message.reject(requeue=True)
            error = True
        try:
            result = self.callback(candles)
        except Exception as e:
            app_logger.error(f"Process message error: {e}")
            message.reject(requeue=True)
            error = True
        if not error:
            # put results to redis
            self._save_to_redis(candles.ticker, result)
            # ack message
            message.ack()


    def _save_to_redis(self, ticker: str, result: AnalysisModel):
        self.redis_con.set(
            ticker,
            result.json()
        )
        app_logger.info(f"Consumer push analysis results to redis, ticker: {ticker}.")


    def callback(self, candles: CandlesModel) -> AnalysisModel:
        
        # ML HERE

        opens_list = [candle.openPrice for candle in candles.data]
        close_list = [candle.closePrice for candle in candles.data]

        return AnalysisModel(
            timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            hurst = HurstModel(
                openPrice = HurstPeriodValue(
                    day = get_hurst_exponent(opens_list[-100:]), # (24 * 60) / 15
                    week = get_hurst_exponent(opens_list)
                ),
                closePrice = HurstPeriodValue(
                    day = get_hurst_exponent(close_list[-100:]),
                    week = get_hurst_exponent(close_list)
                )
            ),
            predictions = [],
        )