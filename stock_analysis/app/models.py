from typing import Any, List
from pydantic import BaseModel



class SingleCandleModel(BaseModel):
    time: str
    openPrice: float
    highPrice: float
    lowPrice: float
    closePrice: float
    volume: int


class CandlesModel(BaseModel):
    ticker: str
    data: List[SingleCandleModel]


class HurstPeriodValue(BaseModel):
    day: float
    week: float


class HurstModel(BaseModel):
    openPrice: HurstPeriodValue
    closePrice: HurstPeriodValue


class AnalysisModel(BaseModel):
    timestamp: str
    hurst: HurstModel
    predictions: List[Any]

