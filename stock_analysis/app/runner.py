import redis
from kombu import Connection, Queue
from .consumer import SAConsumer
from . import app_settings, app_logger


def run_consumer():
    queue = Queue(app_settings.rabbit_queue)
    with Connection(
                f"amqp://{app_settings.rabbit_user}:{app_settings.rabbit_password}@{app_settings.rabbit_host}:{app_settings.rabbit_port}/",
                heartbeat=60
            ) as conn:
        app_logger.info("RabbitMQ connection established.")
        redis_con = redis.Redis(
            host=app_settings.redis_host,
            port=app_settings.redis_port
        )
        app_logger.info("Redis connection established.")
        app_logger.info("Creating consumer...")
        con = SAConsumer(conn, redis_con, queue)
        app_logger.info("Starting consumer...")
        con.run()
        app_logger.info("Consumer stopped.")