from datetime import datetime
import yfinance
from typing import Dict, Union
from .models import SingleCandleModel, TickerHistoryModel


def yfinance_fetcher(
            ticker: str, 
            data_from: datetime, 
            data_interval: str # 1m,2m,5m,15m,30m,60m,90m,1h,1d,5d,1wk,1mo,3mo
        ) -> Union[TickerHistoryModel, datetime]:
    data = yfinance.download(
        ticker,
        start=data_from,
        interval=data_interval
    )
    candles = [
        SingleCandleModel(
            time = rtime.tz_convert(tz="Europe/Warsaw").isoformat(),
            openPrice = ropen,
            highPrice = rhigh, 
            lowPrice = rlow, 
            closePrice = rclose, 
            volume = rvolume
        ) for (rtime, ropen, rhigh, rlow, rclose, _, rvolume) in data.itertuples(name=None)
    ]
    return (
        TickerHistoryModel(
            ticker=ticker, 
            date_from=candles[0].time if len(candles) > 0 else None,
            date_to=candles[-1].time if len(candles) > 0 else None,
            interval=data_interval,
            candles=candles
        ),
        (datetime.fromisoformat(candles[-1].time) if len(candles) > 0 else data_from)
    )
    

tickers_api_fetchers: Dict = {
    "yfinance": {
        "function": yfinance_fetcher,
        "interval": "15m"
    }
}