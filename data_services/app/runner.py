import asyncio
import json
import uvicorn
import aio_pika
from fastapi import FastAPI, APIRouter, HTTPException
from datetime import datetime, timedelta
from typing import Any, Callable, Dict, Union, List
from dataclasses import dataclass

from .models import TickerHistoryModel, TickerInfoModel
from . import app_settings, app_logger
from .publisher import publish_results
from .fetchers import tickers_api_fetchers
from .info_providers import infpro_tickers


@dataclass
class TickerFetcher:
    ticker: str
    fetch_function: Callable[[str, datetime, str], Union[TickerHistoryModel, datetime]]
    fetch_interval: Any
    last_fetch: datetime
    channel: aio_pika.Channel


    async def fetch_forever(self):
        while True:
            app_logger.info(f"Start fetching for ticker: {self.ticker}.")
            results, last_fetch = self.fetch_function(
                self.ticker,
                self.last_fetch,
                self.fetch_interval
            )
            app_logger.info(f"Fetched {len(results.candles)} for ticker: {self.ticker}")
            if len(results.candles) > 0:
                self.last_fetch = last_fetch + timedelta(seconds=1)
                await publish_results(results, self.channel)
            await asyncio.sleep(app_settings.fetcher_sleep)


def init_ticker_fetchers(channel: aio_pika.Channel) -> List[TickerFetcher]:
    with open(app_settings.tickers_json) as file:
        stock_tickers = json.loads(file.read())
        
        fetch_sheduler = []
        for api_name, tickers in stock_tickers.items():
            for ticker in tickers:
                fetch_sheduler.append(TickerFetcher(
                    ticker = ticker,
                    fetch_function=tickers_api_fetchers[api_name]["function"],
                    last_fetch=datetime.strptime(app_settings.fetch_from, "%Y-%m-%d %H:%M:%S"),
                    channel=channel,
                    fetch_interval=tickers_api_fetchers[api_name]["interval"]
                ))
        return fetch_sheduler


async def run_ticker_fetchers(loop):
    rb_connection = await aio_pika.connect_robust(url=app_settings.rb_connection_str, loop=loop)
    app_logger.info("Rabbit connection initialized.")

    async with rb_connection: 
        rb_channel = await rb_connection.channel()

        ticker_fetchers = init_ticker_fetchers(rb_channel)
        app_logger.info("Fetchers initialized.")

        await asyncio.gather(*[tf.fetch_forever() for tf in ticker_fetchers])


def run_data_fetcher():
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run_ticker_fetchers(loop))
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()


def ticker_info_providers_mapping():
    with open(app_settings.tickers_json) as file:
        stock_tickers = json.loads(file.read())
        info_provider_mappings = []
        for k, v in stock_tickers.items():
            info_provider_mappings.append((
                set(v),
                infpro_tickers[k]
            ))
        return info_provider_mappings


info_provider_router = APIRouter()
info_provider_mappings = ticker_info_providers_mapping()


@info_provider_router.get("/info/ticker/{ticker}", response_model=TickerInfoModel)
async def get_ticker_info(
            ticker: str
        ):
    for ts, fun in info_provider_mappings:
        if ticker in ts:
            return fun(ticker)
    raise HTTPException(status_code=404, detail="Ticker not in valid ticker list.")


def run_data_info_provider():
    fa = FastAPI()
    fa.include_router(info_provider_router)
    uvicorn.run(
        fa, 
        host="0.0.0.0", 
        port=app_settings.infopro_port,
        log_level="debug" if app_settings.debug else "info"
    )

     



    