from pydantic import BaseModel
from typing import List, Optional


class SingleCandleModel(BaseModel):
    time: str
    openPrice: float
    highPrice: float
    lowPrice: float
    closePrice: float
    volume: int


class TickerHistoryModel(BaseModel):
    ticker: str
    date_from: Optional[str]
    date_to: Optional[str]
    interval: str
    candles: List[SingleCandleModel]
    

class TickerInfoModel(BaseModel):
    country: Optional[str]
    currency: Optional[str]
    exchange: Optional[str]
    ipo: Optional[str] 
    name: Optional[str]
    phone: Optional[str] 
    sharesOutstanding: Optional[str]
    ticker: Optional[str] 
    logo: Optional[str] 
    industry: Optional[str] 
    fiftyTwoWeekLow: Optional[float] 
    beta: Optional[float] 
    bookValuePerShareAnnual: Optional[float]
    bookValuePerShareQuarterly: Optional[float]
    bookValueShareGrowth5Y: Optional[float]
    dividendPerShareAnnual: Optional[float]
    epsGrowth5Y: Optional[float]
    marketCapitalization: Optional[float]
    epsNormalizedAnnual: Optional[float]
    totalDebtOverTotalEquityQuarterly: Optional[float]
    # dodatkowo
    website: Optional[str]
    business_summary: Optional[str]
    city: Optional[str]

