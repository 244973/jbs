import aio_pika
from .models import TickerHistoryModel
from . import app_logger, app_settings


async def publish_results(results: TickerHistoryModel, channel: aio_pika.Channel):
    r_json = results.json()

    app_logger.info(f"Atempt to publish results for ticker: {results.ticker}")

    await channel.default_exchange.publish(
        aio_pika.Message(
            body=r_json.encode()
        ),
        routing_key=app_settings.rb_queue
    )

    app_logger.debug(f"Results published for ticker: {results.ticker}\n\
        Enities length: {len(results.candles)}\n\
        First enity: {results.candles[0] if len(results.candles) > 0 else '-'}\n\
        Last enity: {results.candles[-1] if len(results.candles) > 1 else '-'}"
    )