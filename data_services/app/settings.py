from typing import Dict
from pydantic import BaseSettings


class Settings(BaseSettings):
    debug: bool = True

    fetcher_sleep: int = 120
    fetch_from: str = "2021-10-20 21:56:00" # YYYY-MM-DD HH:MM:SS
    
    tickers_json: str = "tickers.json"

    rb_host: str = "127.0.0.1"
    rb_port: str = "5672"
    rb_user: str = "guest"
    rb_password: str = "guest"
    rb_queue: str = "TickerData"

    infopro_port: int = 5059

    @property
    def rb_connection_str(self):
        return f"amqp://{self.rb_user}:{self.rb_password}@{self.rb_host}:{self.rb_port}/"