import logging
from .settings import Settings

app_settings: Settings = Settings()

app_logger: logging.Logger = logging.getLogger(name = "app_logger")
app_logger.setLevel(level = logging.DEBUG if app_settings.debug else logging.INFO)
sh = logging.StreamHandler()
sh.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
app_logger.addHandler(sh)