from typing import Dict
import yfinance

from .models import TickerInfoModel


def yfinance_info_provider(ticker: str) -> TickerInfoModel:
    ticker_info = yfinance.Ticker(ticker).info
    return TickerInfoModel(
        country = ticker_info.get("country", None),
        currency = ticker_info.get("financialCurrency", None),
        exchange = ticker_info.get("exchange", None),
        ipo = None,
        name = ticker_info.get("longName", None),
        phone = ticker_info.get("phone", None),
        sharesOutstanding = ticker_info.get("sharesOutstanding", None),
        ticker = ticker_info.get("symbol", None),
        logo = ticker_info.get("logo_url", None),
        industry = ticker_info.get("industry", None),
        fiftyTwoWeekLow = ticker_info.get("fiftyTwoWeekLow", None),
        beta = ticker_info.get("beta", None),
        bookValuePerShareAnnual = None,
        bookValuePerShareQuarterly = None,
        bookValueShareGrowth5Y = None,
        dividendPerShareAnnual = None,
        epsGrowth3Y = None,
        marketCapitalization = None,
        epsNormalizedAnnual = None,
        totalDebtOverTotalEquityQuarterly = None,
        # dodatkowo
        website = ticker_info["website"],
        business_summary = ticker_info["longBusinessSummary"],
        city = ticker_info["city"]
    )


infpro_tickers: Dict = {
    "yfinance": yfinance_info_provider
}